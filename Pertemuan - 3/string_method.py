

# Misalkan expect inputnya seperti "Kucing Bambang"
jenis_nama = input("apa jenis hewan peliharaan: ")

# cara 1
# akan menghasilkan list
detail = jenis_nama.split()
# dari sini baru kalian bisa dapetin hewan dan namanya
hewan = detail[0]
nama = detail[1]

"""
# cara 2
# langsung di assign ke variabelnya
hewan, nama = jenis_nama.split()
"""

#agar memastikan semua pada koridor kasus kita jadi kita masukkan ke lower aja

if hewan.lower() == "kucing" :
    print("kucing bernama", nama)
elif hewan.lower() == "anjing" :
    print("anjing bernama", nama)
else :
    print("hewan bernama", nama)






