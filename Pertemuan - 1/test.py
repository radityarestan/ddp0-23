# Mengimpor module turtle
import turtle

# Mengganti bentuk arrow menjadi kura-kura
turtle.shape("turtle")

# Mengubah Kecepatan menggambar
turtle.speed(3)

# Memilih warna
turtle.fillcolor("orange")

# Membuat persegi
turtle.begin_fill()
turtle.forward(300)
turtle.right(90)
turtle.forward(300)
turtle.right(90)
turtle.forward(300)
turtle.right(90)
turtle.forward(300)
turtle.end_fill()

# Membuat persegi ke-2
turtle.right(93)
turtle.fillcolor("blue")
turtle.begin_fill()
turtle.forward(300)
turtle.right(90)
turtle.forward(300)
turtle.right(90)
turtle.forward(300)
turtle.right(90)
turtle.forward(300)
turtle.end_fill()

# Membuat gui keluar jika diclick
turtle.exitonclick()