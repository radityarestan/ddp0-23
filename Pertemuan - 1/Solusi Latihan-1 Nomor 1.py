# Mengimpor module
import turtle

# Membuat bentuk kura-kura dan memilih warna
turtle.shape("turtle")
turtle.fillcolor("orange")
turtle.begin_fill()

# Membuat garis untuk trapesium kecuali yang sisi miring
turtle.forward(100)
turtle.left(90)
turtle.forward(30)
turtle.left(90)
turtle.forward(60)

# Buat yang sisi miring agak tricky
# Kalian bisa menggunakan konsep matematika biasa bahwa sisi selain sisi miring panjangnya sudah 30 dan 40
# Maka sisi miringnya pasti 50
# Kita tahu bahwa sudut yang dibentuk dari tinggi dan sisi miring itu 53 derajat
# Maka kita cukup membelokkan turtle sebesar 90 - 53 yaitu 37 derajat

turtle.left(37)
turtle.forward(50)
turtle.end_fill()
turtle.exitonclick()

