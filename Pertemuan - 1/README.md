#Pertemuan-1

Pada pertemuan kali ini, materi yang akan dibahas :
1. Perkenalan masing-masing pribadi
2. Menginstall python
3. Data types & Operation
4. Bermain dengan turtle
5. Text Editor & IDE
6. File management
7. Comparison
8. Branching

Source :
1. https://www.notion.so/Week-0-Preparation-43a63cbc030a4d69bee65a842fc990d5
2. https://www.notion.so/Week-1-Introduction-b3d15189f65a444e8d35b40f2f0d522f