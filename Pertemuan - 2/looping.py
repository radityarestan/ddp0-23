for angka in range(10):
    if angka % 2 == 0 :
        print("ini bilangan genap")
    else :
        print("Ini bilangan ganjil")


for angka in range(0, 11, 2):
    print(angka)


nama = "Bambang"
for i in range(len(nama)) :
    print("Hai")

list_1 = [1,2,3]

for indeks in range(len(list_1)):
    print(list_1[indeks])
else :
    print("sudah selesai")


angka = 0
while angka < 11 :
    print(angka)
    angka += 1

# Memberhentikan secara paksa
for angka in range(10):
    if angka == 5 :
        break
    print(angka)

# Melewatkan suatu kasus
for angka in range(10):
    if angka == 5 :
        continue
    print(angka)

# Kalo misalnya kalian bingung statement yang akan dijalankan
for angka in range(10):
    if angka == 5 :
        pass
    print(angka)
