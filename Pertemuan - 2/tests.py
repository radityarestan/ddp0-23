"""
my_str = "Hello World"
print(my_str[6:10])
print(my_str[6:])
print(my_str[:5])
print(my_str[::2])
print(my_str[::-1])
print(my_str[11:14])

pi = 3.14
var = 5
print(type(var))
print(type(pi))

print(pi / var)

apel = "Rp 3000"
harga_apel_str = apel[3:]
print(type(harga_apel_str))
print(int(harga_apel_str) * 5)

b = 3.5
c = 3.9

print(int(b))
print(int(c))
"""
