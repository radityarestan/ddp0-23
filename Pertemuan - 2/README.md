#Pertemuan ke-2 

Pada pertemuan kali ini, materi yang akan dibahas:
1. Penamaan variabel
2. Slicing
3. Looping
4. Input terminal
5. Tipe data dan type casting
6. Refresher data operator

Source :
1. https://www.notion.so/Week-1-Introduction-b3d15189f65a444e8d35b40f2f0d522f
2. https://www.notion.so/Week-2-Play-Along-df788e72957b43fcb43fbb222447ba34